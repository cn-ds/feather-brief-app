import { Article } from '../../models/article';
import { ApiProvider } from '../api/api';
import { Feeditem } from '../../models/feeditem';

export class ArticlesProvider {

  api : ApiProvider;

  constructor() {
    this.api = new ApiProvider();
  }

  parseArticle(article) : Article {
    return new Article(article.title, article.content, false, false);
  }

  getArticle(article_url : string, markAsRead : boolean, callback) {
    this.api.getJSON(`/article?url=${article_url}`, true, (articleData, err) => {
      if(!err) {
        let article = this.parseArticle(articleData);
        callback(article, false);
        if(markAsRead) {
          this.api.post(`/article/read?url=${article_url}`, {}, true, () => {});
        }
      } else {
        callback(null, true);
      }
    })
  }

  upvoteArticle(article_url : String) {
    this.api.post(`/article/interesting?url=${article_url}`, {}, true, () => {});
  }

  downvoteArticle(article_url : String) {
    this.api.post(`/article/boring?url=${article_url}`, {}, true, () => {});
  }

  saveArticle(article_url : String) {
    this.api.post(`/article/save?url=${article_url}`, {}, true, () => {});
  }
}