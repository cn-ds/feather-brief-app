import { Config } from '../../shared/config';
import axios from 'axios';

export class ApiProvider {

  private url: String = Config.API_ENDPOINT;

  constructor() { }

  getHeader(logged:boolean) {
    if (logged) {
      return {
        headers: {'x-access-token': Config.token}
      };
    } else {
      return {};
    }
  }

  getJSON(endpoint: string, logged:boolean, callback) {
    axios.get(this.url + endpoint,
      this.getHeader(logged)
    )
    .then((response) => {
      return callback(response.data, false);
    })
    .catch(() => {
      return callback(null, true);
    });
  }

  post(endpoint: string, data:object, logged:boolean, callback) {
    axios({
      method: 'POST',
      url: this.url + endpoint,
      data: data,
      headers: {'x-access-token': Config.token}
    }).then((response) => {
      callback(response.data, false);
    })
    .catch(() => {
      callback(null, true);
    });
  }

  delete(endpoint: string, data:object, logged:boolean, callback) {
    axios({
      method: 'DELETE',
      url: this.url + endpoint,
      data: data,
      headers: {'x-access-token': Config.token}
    }).then((response) => {
      callback(response.data, false);
    })
    .catch(() => {
      callback(null, true);
    })
  }
}
