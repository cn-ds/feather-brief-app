export class Article{
	constructor(
		public title: string,
		public content: string,
		public saved: boolean,
		public shared: boolean
		) { }
}