import { Component, OnInit } from '@angular/core';
import { NavController} from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(public navCtrl: NavController, public storage: Storage) {  }
  
  ngOnInit() {
  }
}
