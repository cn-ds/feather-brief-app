import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NavController, ToastController} from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Article } from '../../models/article';
import { Storage } from '@ionic/storage';
import { Feeditem } from '../../models/feeditem';
import { ArticlesProvider } from '../../providers/articles/articles';
import { Config } from '../../shared/config';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

  //@ViewChild(Content) content: Content;

  url: string;
  article : Article;
  item : Feeditem;
  saved : boolean;
  progress : Number;
  clipboard : Clipboard;
  toastCtrl : ToastController;
  browser : InAppBrowser;
  articlesProvider : ArticlesProvider;

  constructor(private route: ActivatedRoute, toastCtrl: ToastController, private iab: InAppBrowser, private storage: Storage, private zone: NgZone) {
    this.article = new Article('Waiting...', 'Wait', false, false);
    this.toastCtrl = toastCtrl;
    this.browser = iab;
    this.progress = 0;
    this.articlesProvider = new ArticlesProvider();
  }

  ngOnInit() {
    this.url = this.route.snapshot.paramMap.get('url');
    this.saved = this.route.snapshot.paramMap.get('saved') == 'true' ? true : false;
    if (this !== null && this.saved) {
      this.storage.get(this.url).then((articleParsed) => {
        if (articleParsed !== null) {
          this.article = JSON.parse(articleParsed)
        } else {
          this.retrieveArticle();
        }
      });
    } else {
      this.retrieveArticle();
    }
  }

  /* updateProgressBar() {
    this.zone.run(() => {
      var element = document.getElementById("content-size");
      var a = this.content.scrollTop + this.content.contentHeight;
      var b = element.offsetHeight;
      this.progress = (a / b) * 100;
    });
  } */

  retrieveArticle() {
    this.articlesProvider.getArticle(this.url, true, (article, err) => {
      if (!err) {
        this.article = article;
        if (this.saved) {
          this.storage.set(this.item.article_url, JSON.stringify(this.article));
        }
      }
    });
  }
  bookmarkArticle(article: Article) {
    this.articlesProvider.saveArticle(this.url);
    this.article.saved = true;
    this.createToast('Article saved');
    this.storage.set(this.url, JSON.stringify(article));
  }

  openInBrowser(article: Article) {
    this.browser.create(this.url, '_system', 'location=yes');
  }

  copyUrl(article: Article) {
    this.article.shared = true;
    this.clipboard = new Clipboard();
    this.clipboard.copy(Config.SHARE_URL + encodeURIComponent(this.url));
    this.createToast('Link copied');
  }

  async createToast(text : string) {
    let toast = await this.toastCtrl.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}
