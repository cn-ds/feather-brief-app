import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  { path: 'feeditems', loadChildren: './feeditems/feeditems.module#FeeditemsPageModule' },
  { path: 'article/:url/:saved', loadChildren: './article/article.module#ArticlePageModule' },
  { path: 'feeds', loadChildren: './feeds/feeds.module#FeedsPageModule' },
  { path: 'offline', loadChildren: './offline/offline.module#OfflinePageModule' },
  { path: 'parameters', loadChildren: './parameters/parameters.module#ParametersPageModule' },
  { path: 'saved', loadChildren: './saved/saved.module#SavedPageModule' },
  { path: 'feed-details/:feed', loadChildren: './feed-details/feed-details.module#FeedDetailsPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
