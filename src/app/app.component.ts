import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { Router} from '@angular/router';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Feed items',
      url: '/feeditems',
      icon: 'paper'
    },
    {
      title: 'Feeds',
      url: '/feeds',
      icon: 'list'
    },
    {
      title: 'Saved items',
      url: '/saved',
      icon: 'bookmark'
    },
    {
      title: 'Offline',
      url: '/offline',
      icon: 'pricetags'
    },
    {
      title: 'Parameters',
      url: '/parameters',
      icon: 'cog'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
    public storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.storage.get('username').then((username) => {
        if (username !== null) {
          this.router.navigateByUrl('/feeditems');
        }
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
