import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeditemsPage } from './feeditems.page';

describe('FeeditemsPage', () => {
  let component: FeeditemsPage;
  let fixture: ComponentFixture<FeeditemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeditemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeditemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
