import { Component, OnInit } from '@angular/core';
import { Feeditem } from '../../models/feeditem';
import { Storage } from '@ionic/storage';
import { FeeditemsProvider } from '../../providers/feeditems/feeditems';
import { ArticlesProvider } from '../../providers/articles/articles';
import { NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-feed-details',
  templateUrl: './feed-details.page.html',
  styleUrls: ['./feed-details.page.scss'],
})
export class FeedDetailsPage implements OnInit {

  feedname : String;
  feeditemsProvider : FeeditemsProvider;
  feeditems : Feeditem[];
  articlesProvider : ArticlesProvider;
  storage : Storage;

  constructor(private route: ActivatedRoute, public navCtrl: NavController, public storageParam: Storage, private toastCtrl: ToastController) {
    this.articlesProvider = new ArticlesProvider();
    this.storage = storageParam;
    this.feeditemsProvider = new FeeditemsProvider();
  }

  ngOnInit(){
    this.feedname = this.route.snapshot.paramMap.get('feed');
    this.feeditemsProvider.getUnreadFeeditemsByFeed(this.feedname, (feeditems) => {
      this.feeditems = feeditems;
    });
  }

  openArticle(item: Feeditem) {
    this.navCtrl.navigateForward(['/article', item.article_url, false]);
    this.feeditems.forEach((feeditem) => {
      if(feeditem.article_url === item.article_url) {
        feeditem.shown = true;
      }
    })
  }

  upvoteArticle(item: Feeditem) {
    this.articlesProvider.upvoteArticle(item.article_url);
    this.createToast('Article upvoted');
  }

  downvoteArticle(item: Feeditem) {
    this.articlesProvider.downvoteArticle(item.article_url);
    this.createToast('Article downvoted');
  }

  bookmarkArticle(item: Feeditem) {
    this.articlesProvider.saveArticle(item.article_url);
    // false is for don't mark as read
    this.articlesProvider.getArticle(item.article_url, false, (article, err) => {
      if(!err) {
        this.createToast('Article saved');
        // Saving the article in the LocalStorage
        this.storage.set(item.article_url, JSON.stringify(article));
      }
    });
  }

  async createToast(text : string) {
    let toast = await this.toastCtrl.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}
