import { Component, OnInit } from '@angular/core';
import { Feed } from '../../models/feed';
import { FeedsProvider } from '../../providers/feeds/feeds';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.page.html',
  styleUrls: ['./feeds.page.scss'],
})
export class FeedsPage implements OnInit {
  feeds: Feed[];
  feedsProvider : FeedsProvider;

  constructor(public navCtrl: NavController) {
    this.feedsProvider = new FeedsProvider();
  }

  ngOnInit(){
    this.feedsProvider.getFeeds((feeds : Feed[], err) => {
      if (!err) {
        this.feeds = feeds;
      }
    });
  }

  openFeed(item: Feed) {
    this.navCtrl.navigateForward(['/feed-details', item.name]);
  }

  unsubscribe(feed : Feed) {
    this.feedsProvider.unsubscribe(feed);
    this.feeds = this.feeds.filter(singleFeed => 
      singleFeed.url !== feed.url
    );
  }
}
