import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { Feed } from '../../models/feed';
import { Config } from '../../shared/config';
import { Util } from '../../shared/util';
import { SettingsProvider } from './../../providers/settings/settings';
import { Storage } from '@ionic/storage';
import { FeedsProvider } from '../../providers/feeds/feeds';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.page.html',
  styleUrls: ['./parameters.page.scss'],
})
export class ParametersPage implements OnInit {
  connected : boolean;
  private storage: Storage;
  token : string;
  username : string;
  password : string;
  feed : Feed;
  feeds : Feed[];
  selectedTheme: String;
  settings: SettingsProvider;
  feedsProvider : FeedsProvider;

  constructor(storage: Storage) {
    this.connected = false;
    this.feed = new Feed("", "");
    this.feeds = new Array();
    this.storage = storage;
    this.connected = Config.authenticated;
    this.feedsProvider = new FeedsProvider();
  }
  
  ngOnInit() {
  }

  connect() {
    console.log("connect(...)");
    Util.connect(this.username, this.password, this.storage).then(() => {
      if (Config.authenticated) {
        this.connected = true;
      }
    })
  }

  subscribe() {
    this.feedsProvider.subscribe(this.feed);
    this.feed = new Feed("", "");
  }

  disconnect() {
    console.log("disconnect(...)");
    this.connected = false;
    this.storage.remove('username');
    this.storage.remove('password');
  }
}
