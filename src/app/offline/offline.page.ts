import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Feeditem } from '../../models/feeditem';

@Component({
  selector: 'app-offline',
  templateUrl: './offline.page.html',
  styleUrls: ['./offline.page.scss'],
})
export class OfflinePage implements OnInit {
  feeditems: Feeditem[];
  lastDate: number;
  storage : Storage;
  shownFeeditem : String[];

  constructor(public navCtrl: NavController, public storageParam: Storage) {
    this.storage = storageParam;
    this.feeditems = [];
  }

  ngOnInit(){
    this.storage.forEach((value, key, i) => {
      if (key !== 'username' && key !== 'password' && key !== 'theme') {
        const article = JSON.parse(value);
        const feeditem = new Feeditem(key, article.title, '', '', '', 0, '', 0, false, 0);
        this.feeditems.push(feeditem);
      }
    });
  }

  openArticle(item: Feeditem) {
    this.navCtrl.navigateForward(['/article', item.article_url, true]);
    this.feeditems.forEach((feeditem) => {
      if(feeditem.article_url === item.article_url) {
        feeditem.shown = true;
      }
    })
  }
}
