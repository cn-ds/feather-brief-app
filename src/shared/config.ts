export class Config {
    public static API_ENDPOINT = "https://cn-ds.fr";
    public static token;
    public static authenticated = false;
    public static SHARE_URL = "https://cn-ds.gitlab.io/web-app/#/article/";
}