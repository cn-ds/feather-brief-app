import { Config } from '../shared/config';
import axios from 'axios';

export class Util {

    static connect(user, pass, storage) {
        return new Promise((resolve, reject) => {
            axios.post(Config.API_ENDPOINT + '/login', {
                username: user,
                password: pass
            })
            .then((response) => {
                Config.authenticated = response.data.auth;
                Config.token = response.data.token;
                if (Config.authenticated) {
                    storage.set('username', user);
                    storage.set('password', pass);
                }
                resolve();
            });
        });
    }
}